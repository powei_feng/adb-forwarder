var express = require('express')
var app = express()
var adb = require('adbkit')
var adbClient = adb.createClient()
var bodyParser = require('body-parser')
var fs = require('fs');
var Jimp = require("jimp");
var net = require('net');
const request = require('request');
const spawn = require('child_process').spawn;
const PassThrough = require('stream').PassThrough;

const configureJimp = require('@jimp/custom');
configureJimp({ plugins: [require('@jimp/plugin-threshold')] }, Jimp)

const KEYCODE_BACK            = 4;
const KEYCODE_DPAD_UP         = 19;
const KEYCODE_DPAD_DOWN       = 20;
const KEYCODE_DPAD_LEFT       = 21;
const KEYCODE_DPAD_RIGHT      = 22;
const KEYCODE_DPAD_CENTER     = 23;
const KEYCODE_ENTER           = 66;
const KEYCODE_DEL             = 67;
const KEYCODE_SPACE           = 62;
const KEYCODE_HOME            = 3;
const KEYCODE_VOLUME_UP       = 24;
const KEYCODE_VOLUME_DOWN     = 25;

const GOOD = "good";
const WAIT = "wait";
const ERROR = "error";
const UNKNOWN = "unknown_key";

const SCREEN_MIRROR_PKG = 'com.forwarder.screenmirror';
const SCREEN_MIRROR_APK_LOC = './screen_mirror/app/build/outputs/apk/debug/app-debug.apk';
//const SCREEN_MIRROR_APK_LOC = './screen_mirror/app/build/outputs/apk/release/app-release-unsigned.apk';
const SCREEN_MIRROR_PORT = 4322;
const SCREEN_MIRROR_LABEL = "screenmirror";

const LOCAL_HOST = '127.0.0.1';

const str = (input) => {
  return Promise.resolve(adb.util.readAll(input).then(i => i.toString()));
};

const hashCode = function(str) {
  var hash = 0, i, chr;
  if (str.length === 0) return hash;
  for (i = 0; i < str.length; i++) {
    chr   = str.charCodeAt(i);
    hash  = ((hash << 5) - hash) + chr;
    hash |= 0; // Convert to 32bit integer
  }
  return hash;
};

const getPort = (deviceId) => {
  var code = hashCode(deviceId) % 1000;
  return 5000 + hashCode(deviceId) % 1000;
};

const getPort2 = (deviceId) => {
  var code = hashCode(deviceId) % 1000;
  return 4000 + hashCode(deviceId) % 1000;
};

const tapOnScreen = (client, x, y) => {
  x = Math.round(x);
  y = Math.round(y);
  return new Promise((ok, fail) => {
    client.tap(x, y, (err) => ok(err ? ERROR : GOOD));
  });
};

const monkeyTemplate = (client, write) => {
  return {
    client: client,
    keyDown: (key, cb) => write('key down', key, cb),
    keyUp: (key, cb) => write('key up', key, cb),
    press: (key, cb) => write('press', key, cb),
    tap: (x, y, cb) => write('tap', x + ' ' + y, cb),
    type: (string, cb) => write('type', string, cb)
  };
};

const fakeMonkey = (client) => {
  const passThroughWrite = (cmd, key, cb) => cb();
  var ret = monkeyTemplate(client, passThroughWrite);
  ret.isFake = true;
  return ret;
};

const endMonkey = (SERIAL) => {
  if (!_monkeyClients[SERIAL]) return;

  if (_monkeyClients[SERIAL].client) {
    _monkeyClients[SERIAL].client.end();
  }
  _monkeyClients[SERIAL] = null;
}

var makeMonkey = (SERIAL) => new Promise((ok, fail) => {
  try {
    const PORT = getPort(SERIAL);
    var client = new net.Socket();
    console.log('make monkey ' + SERIAL + " " + PORT);

    var callbacks = [];
    const popCallback = () => {
      if (callbacks.length == 0) return null;
      var ret = callbacks[0];
      callbacks = callbacks.slice(1);
      return ret;
    };

    client.connect(PORT, LOCAL_HOST, (res) => {
      console.log(SERIAL + ' connected');

      client.on('data', (reply) => {
        reply = reply.toString().trim();
        console.log('monkey reply', reply);

        var callback = popCallback();
        if (!callback) return;

        if (reply == 'OK') {
          callback();
        } else {
          callback(reply);
        }
      });

      const write = (cmd, str, cb) => {
        callbacks.push(cb);
        client.write(cmd + ' ' + str + '\n');
      };

      ok(monkeyTemplate(client, write));
    });

    client.on('error', err => {
      console.log('monkey port: ' + PORT + ' error ' + err);
      var callback = popCallback();
      if (callback) callback(err);
      endMonkey(SERIAL);
    });

    client.on('end', (err) => {
      console.log('monkey port: ' + PORT + ' ended ' + err);
      var callback = popCallback();
      if (callback) callback(err);
      endMonkey(SERIAL);
    });

    client.on('close', (err) => {
      console.log('monkey port: ' + PORT + ' closed ' + err);
      var callback = popCallback();
      if (callback) callback(err);
      endMonkey(SERIAL);
    });
  } catch (err) {
    console.log('failed to make monkey: ' + err);
    fail(err);
  }
});

var _monkeyClients = {};
var _coords = {};
var _screenMirrorClients = {};
var _waitingForScreen = {};

exports.start = (config) => {
  const HOME = 36;
  const SPACE = 32;
  const ENTER = 13;
  const ESCAPE = 27;
  const BACKSPACE = 8;
  const LEFT = 37;
  const UP = 38;
  const RIGHT = 39;
  const DOWN = 40;
  const PAGE_UP = 33;
  const PAGE_DOWN = 34;

  const CTRL_ENTER = 1337;

  const SPEC_KEYS = {};
  SPEC_KEYS[SPACE] = KEYCODE_SPACE;
  SPEC_KEYS[ENTER] = KEYCODE_DPAD_CENTER;
  SPEC_KEYS[ESCAPE] = KEYCODE_BACK;
  SPEC_KEYS[BACKSPACE] = KEYCODE_DEL;
  SPEC_KEYS[LEFT] = KEYCODE_DPAD_LEFT;
  SPEC_KEYS[RIGHT] = KEYCODE_DPAD_RIGHT;
  SPEC_KEYS[UP] = KEYCODE_DPAD_UP;
  SPEC_KEYS[DOWN] = KEYCODE_DPAD_DOWN;
  SPEC_KEYS[HOME] = KEYCODE_HOME;

  SPEC_KEYS[PAGE_UP] = KEYCODE_VOLUME_UP;
  SPEC_KEYS[PAGE_DOWN] = KEYCODE_VOLUME_DOWN;


  SPEC_KEYS[CTRL_ENTER] = KEYCODE_ENTER;

  app.use(bodyParser.json());
  app.use(bodyParser.raw());
  app.use(bodyParser.text());

  const transform = (inkey) => SPEC_KEYS[inkey];
  const transformText = (intext) => {
    if (intext.length > 1) return null;
    return intext;
  };

  const stream2Buf = (stream) => new Promise((ok, fail) => {
    var bufs = [];
    stream.on('data', (d) => { bufs.push(d); });
    stream.on('end', () => {
      ok(Buffer.concat(bufs));
    });
  });

  const processImg = (buf) => new Promise((ok, fail) => {
    Jimp.read(buf, (err, img) => {
      img.greyscale()
          .invert()
          .threshold({max: 150})
            .getBuffer('image/jpeg', (err, buf2) => ok(buf2));
    });
  });

  const ocr = (buf) => new Promise((ok, fail) => {
    let child = spawn('tesseract', ['stdin', 'stdout', 'tsv']);
    child.stdin.write(buf);
    child.stdin.end();
    stream2Buf(child.stdout).then((buf) => {
      ok(buf.toString());
    });
  });

  // result is the tsv format of tesseract
  const checkForMirroringPermissionButton = (result) => new Promise((ok, fail) => {
    const FAILURE_RES = null;
    if (!result || result.length == 0 || result.indexOf('\n') < 0) {
      ok(FAILURE_RES);
      return;
    }
    let lines = result.split('\n').map(r => r.split('\t').map(s => s.toLowerCase()));
    let words = lines.map(line => line[11]);
    let ind = words.indexOf(SCREEN_MIRROR_LABEL);
    let nowInd = words.indexOf('now');
    if (ind >= 0 && nowInd >= 0) {
      let positions = lines.map(
          line =>
            ({left: parseInt(line[6]),
              top: parseInt(line[7]),
              width: parseInt(line[8]),
              height: parseInt(line[9])}));
      ok(positions[nowInd]);
    } else {
      ok(FAILURE_RES);
    }
  });

  const fallback = (SERIAL, res) => {
    return adbClient.shell(SERIAL, 'screencap -p').then((a) => {
      a.pipe(res);
      return a;
    });
  };

  app.get('/devices', (req, res) => {
    adbClient.listDevices().then((devices) => res.send(devices));
  });

  app.get('/screen/:sd', (req, res) => {
    const SERIAL = req.params.sd;

      const pathOne = () => fallback(SERIAL, res)
            .then(stream2Buf)
            .then(processImg)
            .then(ocr)
            .then(checkForMirroringPermissionButton)
            .then(pos => {
              if (!pos) {
                return Promise.resolve(false);
              }
              let x = pos.left;
              let y = pos.top + pos.height/2;
              console.log(x, y);
              return getMonkey(SERIAL).then(
                  client => tapOnScreen(client, x, y).then(r => true));
            }).then(result => {
              console.log('after tap ', result);
              if (result) {
                const PORT = getPort2(SERIAL);
                _screenMirrorClients[SERIAL] = PORT;
                _waitingForScreen[SERIAL] = false;
              }
            });

    const useScreenCap = () => {
      if (_waitingForScreen[SERIAL]) {
        return pathOne();
      }

      _waitingForScreen[SERIAL] = true;
      _screenMirrorClients[SERIAL] = null;
      return enableScreenMirror(SERIAL).then(pathOne);
    };

    if (_screenMirrorClients[SERIAL]) {
      const MIRROR_PORT = _screenMirrorClients[SERIAL];
      request.get("http://" + LOCAL_HOST + ':' + MIRROR_PORT)
          .on('response', (response) => {
            if (response.statusCode == 200) {
              response.pipe(res);
            } else {
              useScreenCap();
            }
          }).on('error', (err) => {
            console.log('e', err);
            useScreenCap();
          });
    } else {
      useScreenCap();
    }
  });

  const getSdkVersion = (serial) => {
    return adbClient.shell(serial, 'getprop ro.build.version.sdk');
  };

  const enableScreenMirror = (SERIAL) => new Promise((ok, fail) => {
    console.log('start mirror');
    const PORT = getPort2(SERIAL);
    adbClient
        .shell(SERIAL, 'am force-stop ' + SCREEN_MIRROR_PKG)
        .then(() => adbClient.install(SERIAL, SCREEN_MIRROR_APK_LOC))
        .then(() => adbClient.shell(SERIAL, 'am start -n ' + SCREEN_MIRROR_PKG + "/.MainActivity"))
        .then(() => adbClient.forward(SERIAL, 'tcp:' + PORT, 'tcp:' + SCREEN_MIRROR_PORT)).then(ok);
  });

  const rebootMonkeyImpl = (SERIAL) => new Promise((ok, fail) => {
    const PORT = getPort(SERIAL);
    console.log('reboot monkey');
    // Keep a temporary stub in place so that multiple initializations cannot happen.
    _monkeyClients[SERIAL] = fakeMonkey(SERIAL);

    adbClient
      .shell(SERIAL, 'getprop ro.build.version.sdk').then(str)
      .then(num => {
        console.log('num: ' + num);
        var psCmd = 'ps | grep monkey';
        if (num >= 26) {
          psCmd = 'ps -A | grep monkey';
        }
        return adbClient.shell(SERIAL, psCmd).then(str);
      })
      .then(output => {
        var lines = output.split('\n').map(s => s.replace(/ +(?= )/g,''));
        var monkeysKilled = [];
        lines.forEach(line => {
          if (line.indexOf('monkey') >= 0) {
            console.log('--- monkey line: ', line);
            var words = line.split(' ');
            monkeysKilled.push(adbClient.shell(SERIAL, 'kill -9 ' + words[1]).then(str));
          }
        });
        if (monkeysKilled.length > 0) {
          return Promise.all(monkeysKilled);
        }
        return Promise.resolve('no monkey');
      })
      .then((output) => {
        console.log('after kill monkey', output);
        return adbClient.forward(SERIAL, 'tcp:' + PORT, 'tcp:' + PORT);
      })
      .then((output) => {
        console.log('after forward ' + output + ' ');
        return adbClient.shell(SERIAL, "monkey --port " + PORT);
      })
      .then((socket) => new Promise((ok, fail) => {
        var alreadyMonkeyed = false;
        socket.on('data', (d) => {
          console.log('socket data: ' + (d ? d.toString() : 'null'));
          var output = d ? d.toString() : '';
          if (!alreadyMonkeyed) {
            alreadyMonkeyed = true;
            setTimeout(() => {
              ok(makeMonkey(SERIAL));
            }, 2000);
          }
        });
        socket.on('end', (d) => {
          endMonkey(SERIAL);
          console.log('socket end: ' + (d ? d.toString() : 'null'));
        });
        socket.on('close', (d) => {
          endMonkey(SERIAL);
          console.log('socket close: ' + (d ? d.toString() : 'null'));
        });
      }))
      .then((monkey) => {
        console.log(monkey.keyDown);
        _monkeyClients[SERIAL] = monkey;
        ok(monkey);
      })
      .catch((err) => {
        endMonkey(SERIAL);
        console.log('reboot monkey error ', err);
        fail(ERROR);
      });
  });

/*  const rebootMonkey = (SERIAL) => new Promise((ok, fail) => {
    rebootMonkeyImpl(SERIAL).then((monkey) => {
      return enableScreenMirror(SERIAL).then(() => ok(monkey));
    });
  });
*/
  const rebootMonkey = rebootMonkeyImpl;

  const getMonkey = (SERIAL) => {
    if (!_monkeyClients[SERIAL]) {
      return rebootMonkey(SERIAL);
    }
    return Promise.resolve(_monkeyClients[SERIAL]);
  };

  app.post('/init', (req, res) => {
    const SERIAL = req.body;
    if (!SERIAL || !SERIAL.length) {
      return res.end(ERROR);
    }
    if (_monkeyClients[SERIAL]) {
      //console.log('already initialized');
      if (_monkeyClients[SERIAL].isFake) {
        console.log('wait');
        return res.end(WAIT);
      } else {
        console.log('good');
        return res.end(GOOD);
      }
    }

    rebootMonkey(SERIAL)
      .then(client => {
        console.log('good');
        res.end(GOOD)
      })
      .catch(err => {
        console.log('reboot monkey error: ' + err);
        console.log('error');
        res.end(ERROR);
      });
  });

  app.post('/key', (req, res) => {
    const info = JSON.parse(req.body);
    const SERIAL = info.device;

    getMonkey(SERIAL).then((monkeyClient) => {
      var nkey = null;
      var ntext = null;
      const onGoodResult = (err) => {
        if (err) console.log(err);
        return res.end(monkeyClient.isFake ? WAIT : GOOD);
      };
      try {
        if (nkey = transform(info.keycode)) {
          if (info.up) {
            return monkeyClient.keyUp(nkey, onGoodResult);
          } else {
            return monkeyClient.keyDown(nkey, onGoodResult);
          }
        } else if (ntext = transformText(info.keycode)) {
          if (!info.up) {
            return monkeyClient.type(ntext, onGoodResult);
          } else {
            return onGoodResult();
          }
        } else {
          return res.end(UNKNOWN);
        }
      } catch (e) {
        console.log('key error 1: ' + e);
        endMonkey(SERIAL);
        _coords[SERIAL] = null;
        res.end(ERROR);
      }
    }).catch((err) => {
      console.log('key error 2: ' + err);
      res.end(ERROR);
    });
  });

  const getCoord = (SERIAL) => {
    return adbClient.shell(
      SERIAL,
      "dumpsys window | grep 'init' |  tr -s ' ' | cut -d' ' -f 2 | sed 's/init=//' | tr 'x' ' '"
    ).then(str)
     .then((output) => {
       var sizes = output.split(' ');
       _coords[SERIAL] = {width: parseInt(sizes[0]), height: parseInt(sizes[1])};
       return _coords[SERIAL];
     });
  };

  app.post('/dim', (req, res) => {
    var SERIAL = req.body;
    getCoord(SERIAL).then((coord) => {
      res.end(JSON.stringify(coord));
    });
  });

  app.post('/pointer', (req, res) => {
    var info = JSON.parse(req.body);
    const SERIAL = info.device;
    getMonkey(SERIAL).then((monkeyClient) => {
      var coord = _coords[SERIAL];
      try {
        if (coord) {
          var dx = info.x * coord.width;
          var dy = info.y * coord.height;
          tapOnScreen(monkeyClient, dx, dy).then(ret => res.end(ret));
        } else {
          getCoord(SERIAL)
            .then(coord => tapOnScreen(monkeyClient, info.x * coord.width, info.y * coord.height))
            .then(ret => res.end(ret));
        }
      } catch (e) {
        console.log('monkey need more time 2', e);
        endMonkey(SERIAL);
        _coords[SERIAL] = null;
        res.end(ERROR);
      }
    }).catch((err) => {
      console.log('key error: ' + err);
      res.end(ERROR);
    });

  });

  app.use(express.static(config.root));
  app.listen(config.port);
  app.on('error', (err) => {
    console.log('app.error', err);
  });

  process.on('uncaughtException', function(err) {
    console.log('process.on handler', err);
  });
};
