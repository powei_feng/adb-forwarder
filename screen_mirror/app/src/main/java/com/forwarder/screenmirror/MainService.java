package com.forwarder.screenmirror;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.PixelFormat;
import android.hardware.display.DisplayManager;
import android.hardware.display.VirtualDisplay;
import android.media.Image;
import android.media.ImageReader;
import android.media.projection.MediaProjection;
import android.media.projection.MediaProjectionManager;
import android.os.Binder;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.WindowManager;
import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.NanoHTTPD.Response.Status;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

public class MainService extends Service {

  private static final String TAG = "forward.service";

  private static final int PORT = 4322;

  public class Server extends NanoHTTPD {

    public Server() throws IOException {
      super(PORT);
      start(NanoHTTPD.SOCKET_READ_TIMEOUT, false);
    }

    @Override
    public Response serve(IHTTPSession session) {
      final String mime = "image/jpeg";
      try {
        synchronized(IMG_LOCK) {
          return newChunkedResponse(Status.OK, mime, new ByteArrayInputStream(mImgBytes));
        }
      } catch (Exception e) {
        Log.e(TAG, "error in reading: " + e);
      }
      return newFixedLengthResponse(Status.INTERNAL_ERROR, "", "something went wrong");
    }
  }

  // https://stackoverflow.com/questions/31255276/android-get-byte-from-surface-virtualdisplay
  public class LocalBinder extends Binder {
    public void okToInit(int resultCode, Intent data) {
      shareScreen(resultCode, data);
      goHome();
    }
  }

  private class MediaProjectionCallback extends MediaProjection.Callback {
    @Override
    public void onStop() {
      mMediaProjection = null;
      stopScreenSharing();
    }
  }

  private int mScreenDensity;
  private MediaProjectionManager mProjectionManager;
  private MediaProjection mMediaProjection;
  private VirtualDisplay mVirtualDisplay;
  private int mDisplayWidth = 960 ;
  private int mDisplayHeight = 540;
  private boolean mScreenSharing = false;
  private LocalBinder mBinder = new LocalBinder();
  private ImageReader mImageReader;

  private Server mServer;
  private HandlerThread mServerThread;
  private Handler mServerHandler;
  private byte[] mImgBytes = null;

  private Object IMG_LOCK = new Object();

  private void goHome() {
    Intent intent = new Intent(Intent.ACTION_MAIN).addCategory(Intent.CATEGORY_HOME);
    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    startActivity(intent);
  }

  private void startServer() {
    try {
      mServer = new Server();
    } catch (IOException e) {
      Log.e(TAG, "failed to instantiate server: " + e);
    }
  }

  public MainService() {}

  @Override
  public void onCreate() {
    super.onCreate();
    Log.e(TAG, "on create");

    mServerThread = new HandlerThread("server-thread");
    mServerThread.start();
    mServerHandler = new Handler(mServerThread.getLooper());
    mServerHandler.post(this::startServer);

    WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
    DisplayMetrics metrics = new DisplayMetrics();
    wm.getDefaultDisplay().getMetrics(metrics);
    mScreenDensity = metrics.densityDpi;

    mImageReader = ImageReader.newInstance(mDisplayWidth, mDisplayHeight, PixelFormat.RGBA_8888, 2);
    mImageReader.setOnImageAvailableListener(new ImageAvailableListener(), null);

    mProjectionManager =
        (MediaProjectionManager) getSystemService(Context.MEDIA_PROJECTION_SERVICE);
  }

  private class ImageAvailableListener implements ImageReader.OnImageAvailableListener {

    @Override
    public void onImageAvailable(ImageReader reader) {
      try {
        Image image = null;
        image = mImageReader.acquireLatestImage();
        if (image != null) {
          Image.Plane[] planes = image.getPlanes();
          ByteBuffer buffer = planes[0].getBuffer();
          int pixelStride = planes[0].getPixelStride();
          int rowStride = planes[0].getRowStride();
          int rowPadding = rowStride - pixelStride * mDisplayWidth;

          // create bitmap
          Bitmap bitmap = Bitmap.createBitmap(mDisplayWidth + rowPadding / pixelStride,
              mDisplayHeight, Bitmap.Config.ARGB_8888);
          bitmap.copyPixelsFromBuffer(buffer);
          ByteArrayOutputStream stream = new ByteArrayOutputStream();
          bitmap.compress(CompressFormat.JPEG, 50, stream);
          synchronized(IMG_LOCK) {
            mImgBytes = stream.toByteArray();
          }
          image.close();
        }

      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    Log.e(TAG, "on destroy");
    if (mMediaProjection != null) {
      mMediaProjection.stop();
      mMediaProjection = null;
    }
  }

  @Override
  public IBinder onBind(Intent intent) {
    return mBinder;
  }

  private void resizeVirtualDisplay() {
    if (mVirtualDisplay == null) {
      return;
    }
    mVirtualDisplay.resize(mDisplayWidth, mDisplayHeight, mScreenDensity);
  }

  private void shareScreen(int resultCode, Intent data) {
    mScreenSharing = true;
    mMediaProjection = mProjectionManager.getMediaProjection(resultCode, data);
    mMediaProjection.registerCallback(new MediaProjectionCallback(), null);
    if (mMediaProjection == null) {
      return;
    }
    mVirtualDisplay = createVirtualDisplay();
  }

  private void stopScreenSharing() {
    mScreenSharing = false;
    if (mVirtualDisplay != null) {
      mVirtualDisplay.release();
      mVirtualDisplay = null;
    }
  }

  private VirtualDisplay createVirtualDisplay() {
    return mMediaProjection.createVirtualDisplay("ScreenSharingDemo",
        mDisplayWidth, mDisplayHeight, mScreenDensity,
        DisplayManager.VIRTUAL_DISPLAY_FLAG_AUTO_MIRROR,
        mImageReader.getSurface(), null /*Callbacks*/, null /*Handler*/);
  }
}
