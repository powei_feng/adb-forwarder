package com.forwarder.screenmirror;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.media.projection.MediaProjectionManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.forwarder.screenmirror.MainService.LocalBinder;

public class MainActivity extends AppCompatActivity {

  private static final String TAG = "forward.activity";
  private static final int PERMISSION_CODE = 12;

  public class Connection implements ServiceConnection {

    @Override
    public void onServiceConnected(ComponentName name, IBinder binder) {
      mBinder = (LocalBinder) binder;
      startActivityForResult(mProjectionManager.createScreenCaptureIntent(),
          PERMISSION_CODE);
    }

    @Override
    public void onServiceDisconnected(ComponentName name) { }
  }

  private MediaProjectionManager mProjectionManager;
  private LocalBinder mBinder;
  private Connection mConnection = new Connection();

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    mProjectionManager =
        (MediaProjectionManager) getSystemService(Context.MEDIA_PROJECTION_SERVICE);

    Context appContext = getApplicationContext();
    Intent mainIntent = new Intent(appContext, MainService.class);
    startService(mainIntent);

    appContext.bindService(new Intent(appContext, MainService.class), mConnection, Context.BIND_AUTO_CREATE);
  }

  @Override
  protected void onStop() {
    super.onStop();
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    if (requestCode != PERMISSION_CODE) {
      Log.e(TAG, "Unknown request code: " + requestCode);
      return;
    }
    if (resultCode != RESULT_OK) {
      Toast.makeText(this,
          "User denied screen sharing permission", Toast.LENGTH_SHORT).show();
      return;
    }
    if (mBinder != null) {
      mBinder.okToInit(resultCode, data);
    }
    super.onActivityResult(requestCode, resultCode, data);
  }
}
