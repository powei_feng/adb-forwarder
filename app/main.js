var RB = RB || {}; // must execute before all imports, makes RB global

import React from 'react';
import ReactDOM from 'react-dom';
import Home from './home.jsx';

import injectTapEventPlugin from 'react-tap-event-plugin';

// Needed for onTouchTap
// Can go away when react 1.0 release
// Check this repo:
// https://github.com/zilverline/react-tap-event-plugin
injectTapEventPlugin();

var mainInit = () => {
    ReactDOM.render(React.createElement(Home), document.getElementById('app'));
}

setTimeout(() => mainInit(), 100);
