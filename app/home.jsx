import React from 'react';
import SelectField from 'material-ui/lib/select-field';
import MenuItem from 'material-ui/lib/menus/menu-item';

const LAST_USED = "last-used";

const NOT_READY_TIMEOUT = 1000;

const WAIT = "wait";
const GOOD = "good";
const KEY = "/key";
const CLICK = "/pointer";
const SCREENSHOT = "/screen";

const LEFT = 37;
const UP = 38;
const RIGHT = 39;
const DOWN = 40;
const HOME = 36;
const SPACE = 32;
const ENTER = 13;
const ESCAPE = 27;
const BACKSPACE = 8;
const MINUS = 189;
const EQUAL = 187;
const CTRL_ENTER = 1337;
const WAITING = 1338;
const PAGE_UP = 33;
const PAGE_DOWN = 34;
const LETTER_H = 72;

const CONTAINER_STYL = {
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
  padding: "10px 5px",
  height: "100%"
};

const softCopy = (dict) => {
  var ret = {};
  for (var i in dict) {
    ret[i] = dict[i];
  }
  return ret;
};

const random = () => Math.round(Math.random() * 10000000);

// Taken from https://stackoverflow.com/questions/14573223/set-cookie-and-get-cookie-with-javascript
function setCookie(name,value,days) {
  var expires = "";
  if (days) {
    var date = new Date();
    date.setTime(date.getTime() + (days*24*60*60*1000));
    expires = "; expires=" + date.toUTCString();
  }
  document.cookie = name + "=" + (value || "")  + expires + "; path=/";
}

function getCookie(name) {
  var nameEQ = name + "=";
  var ca = document.cookie.split(';');
  for(var i=0;i < ca.length;i++) {
    var c = ca[i];
    while (c.charAt(0)==' ') c = c.substring(1,c.length);
    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
  }
  return null;
}

function eraseCookie(name) {
  document.cookie = name+'=; Max-Age=-99999999;';
}

class InputHistoryRow extends React.Component {
  constructor(params) {
    super(params);
  }

  _findIcon(key) {
    switch (key) {
      case LEFT: return 'arrow-left.png';
      case UP: return 'arrow-up.png';
      case RIGHT: return 'arrow-right.png';
      case DOWN: return 'arrow-down.png';
      case HOME: return 'home.png';
      case ESCAPE: return 'back.png';
      case ENTER: return 'dpad_center.png';
      case BACKSPACE: return 'backspace.png';
      case CTRL_ENTER: return 'enter.png';
      case PAGE_UP: return 'volume-up.png';
      case PAGE_DOWN: return 'volume-down.png';
      default:
        return 'circle.png';
    }
  }

  render() {
    const keys = [];
    const len = this.props.input.length;
    for (var i = Math.max(0, len - 10); i < len; i++) {
      (() => {
        var data = this.props.input[i];
        var content = null;
        if (data.click) {
          const imgSrc = './img/touch.png';
          content = (<img style={{width: "100%", height: "100%"}} src={imgSrc}></img>);
        } else if (data.specialKey && data.key != SPACE) {
          const imgSrc = './img/' + this._findIcon(data.key);
          content = (<img style={{width: "100%", height: "100%"}} src={imgSrc}></img>);
        } else {
          var key = data.key;
          const buttonStyl = {
            border: "2px solid black",
            borderRadius: 5,
            padding: 5,
            textAlign: "center",
            fontWeight: 600,
            fontSize: 20};
          if (data.click) {
            content = <div style={buttonStyl}>👆</div>
          } else if (data.key == SPACE) {
            content = <div style={buttonStyl}>&nbsp;</div>
          } else {
            content = <div style={buttonStyl}>{key}</div>
          }
        }
        keys.push((
          <div key={data.id} className="fade" style={{width: 35, height: 35, margin: 10}}>
            {content}
          </div>
        ));
      })();
    }

    return (
      <div style={{display: "flex", flexDirection: "row"}}>
        {keys}
      </div>
    );
  }
}

class Home extends React.Component {
  constructor(params) {
    super(params);
    this.state = {
      status: 'a',
      ready: true,
      devices: [],
      screenshotPath: '',
      selectedDevice: null,
      focused: true,
      windowWidth: window.innerWidth,
      windowHeight: window.innerHeight,
      screenshotWidth: 1,
      screenshotHeight: 1,
      screenshotFactor: 1,
      showNotReadyUi: false,
      inputHistory: []
    }

    this.keyState = {};
    this.notReadyUiCallback = null;
  }

  post(route, data, ignoreReadyState) {
    if (!ignoreReadyState && !this.state.ready) {
      console.log("not ready for post: " + route + " " + data);
      return;
    }

    if (ignoreReadyState) {
      this.setState({ready: true});
    } else {
      this.setState({ready: false});
    }
    data = softCopy(data);
    data.device = this.state.selectedDevice;
    var req = new XMLHttpRequest();
    req.open("POST", route);
    req.send(JSON.stringify(data));
    req.onload = (res) => {
      this.setState({ ready: true});
    };
    req.onerror = (err) => {
      console.log('posting error: ', err);
      this.setState({ ready: true});
      if (req.response != GOOD) {
        this.init();
      }
    };
  };

  componentDidUpdate(prevProps, prevState) {
    const selectedDeviceChanged = this.state.selectedDevice != prevState.selectedDevice;
    const focusChanged = this.state.focused != prevState.focused;

    if (selectedDeviceChanged) {
      setCookie(LAST_USED, this.state.selectedDevice);
      this.init();
    }
    if (selectedDeviceChanged || (focusChanged && this.state.focused)) {
      this.getScreenshotDim().then((coord) => {
        this.setState({
          screenshotWidth: coord.width,
          screenshotHeight: coord.height,
          screenshotPath: this.getScreenshotSrc(),
          ready: this.state.selectedDevice != null
        })
      });
    }

    if (this.state.ready != prevState.ready) {
      if (this.notReadyUiCallback) {
        clearTimeout(this.notReadyUiCallback);
        this.notReadyUiCallback = null;
      }

      if (this.state.ready) {
        this.setState({showNotReadyUi: false});
      } else {
        this.notReadyUiCallback = setTimeout(() => {
          this.setState({showNotReadyUi: true});
          this.notReadyUiCallback = null;
        }, NOT_READY_TIMEOUT);
      }
    }
  }

  init() {
    var req = new XMLHttpRequest();
    req.open("POST", "/init");
    req.send(this.state.selectedDevice);
    this.setState({ready: false});
    req.onload = () => {
      this.setState({ready: req.response == GOOD});
    };
  }

  componentDidMount() {
    const KEYDOWN = "keydown";
    const KEYUP = "keyup";

    const toMetaKeyCode = (keycode, control, shift, alt) => {
      if (keycode == ENTER && control == true) {
        return CTRL_ENTER;
      }
      if (keycode == LETTER_H && alt == true) {
        return HOME;
      }
      return 0;
    };

    const isSpecial = (keycode) => {
      switch (keycode) {
        case UP:
        case DOWN:
        case LEFT:
        case RIGHT:
        case SPACE:
        case HOME:
        case ENTER:
        case ESCAPE:
        case BACKSPACE:
        case PAGE_UP:
        case PAGE_DOWN:
          return true;
      }
      return false;
    };

    const getDevices = () => {
      var req = new XMLHttpRequest();
      req.open("GET", "/devices");
      req.send();
      req.onload = () => {
        var items = JSON.parse(req.response).map((item) => item.id);
        var device = getCookie(LAST_USED);
        if (!device) {
          device = items[0];
        }

        this.setState({
          devices: items,
          selectedDevice: device
        });
      };
    };

    window.addEventListener('focus', () => {
      this.setState({ focused: true });
      document.body.style.backgroundColor = "#ffffff";
      document.body.style.opacity = 1;
    });

    window.addEventListener('blur', () => {
      this.setState({ focused: false });
      document.body.style.backgroundColor = "#cbcbcb";
      document.body.style.opacity = .25;
    });

    window.addEventListener('resize', (ev) => {
      this.setState({
        windowHeight: ev.target.window.innerHeight,
        windowWidth: ev.target.window.innerWidth
      });
    });

    var keyState = this.keyState;

    const processKeyEvent = (e) => {
      keyState[e.keyCode] = e.type;
      if (!this.state.ready) return;

      const metaKeyCode = toMetaKeyCode(e.keyCode, e.ctrlKey, e.shiftKey, e.altKey);
      const special = isSpecial(e.keyCode);
      if (metaKeyCode) {
        this.post(KEY, {keycode: metaKeyCode, up: e.type == KEYUP});
        this.startFastScreen(.1);
      } else if (special) {
        this.post(KEY, {keycode: e.keyCode, up: e.type == KEYUP});
        this.startFastScreen(.1);
      } else {
        this.post(KEY, {keycode: e.key, up: e.type == KEYUP});
      }

      if (e.type == KEYUP) {
        const hkey = {id: random()};
        if (metaKeyCode) {
          hkey.specialKey = true;
          hkey.key = metaKeyCode;
        } else if (special) {
          hkey.specialKey = true;
          hkey.key = e.keyCode;
        } else if (e.key.length < 2) {
          hkey.key = e.key;
        }
        if (hkey.key) {
          const hist = this.state.inputHistory;
          hist.push(hkey);
          this.setState({ inputHistory: hist });
        }
      }
    };

    document.addEventListener(KEYUP, processKeyEvent);

    document.addEventListener(KEYDOWN, (e) => {
      if (e.keyCode == EQUAL || e.keyCode == MINUS) {
        if (!this.state.devices || this.state.devices.length == 0) {
          return;
        }
        var len = this.state.devices.length;
        var index = this.state.devices.indexOf(this.state.selectedDevice);
        var indexNext = ((e.keyCode == MINUS ? index + 1 : index - 1) + len) % len;
        if (index < 0) {
          indexNext = 0;
        }
        this.setState({selectedDevice: this.state.devices[indexNext]});
        return;
      }
      if (keyState[e.keyCode] == KEYDOWN) {
        return;
      }
      processKeyEvent(e);
    });

    getDevices();
  }

  handleDeviceChange(ev, index, value) {
    this.setState({ selectedDevice: value });
  }

  startFastScreen(val) {
    if (val > 1) {
      this.fastScreenTimeout = null;
      return;
    }

    if (this.fastScreenTimeout) {
     clearTimeout(this.fastScreenTimeout);
    } else {
      this.setState({
        screenshotPath: this.getScreenshotSrc()
      });
    }

    this.setState({
      screenshotFactor: Math.min(val, 1),
    });

    this.fastScreenTimeout = setTimeout(() => {
      this.startFastScreen(val + .08);
    }, 500);
  }

  getScreenshotSrc() {
    if (!this.state.focused) return this.state.screenshotPath;
    if (!this.state.selectedDevice) return '';
    return SCREENSHOT + '/'
         + this.state.selectedDevice + '?'
         + 'factor=' + this.state.screenshotFactor + '&'
         + new Date().getTime();
  }

  getScreenshotDim() {
    if (!this.state.selectedDevice) return Promise.resolve({width: 0, height: 0});
    return new Promise((ok, fail) => {
      var req = new XMLHttpRequest();
      req.open("POST", "/dim");
      req.send(this.state.selectedDevice);
      req.onload = () => {
        ok(JSON.parse(req.response));
      };
    });
  }

  onLoad(ev) {
    this.setState({
      screenshotPath: this.getScreenshotSrc()
    });
  }

  onImgClick(a, b, ev) {
    if (!this.state.ready) return;

    var dx = a.clientX - a.target.offsetLeft;
    var dy = a.clientY - a.target.offsetTop;
    var width = a.target.clientWidth;
    var height = a.target.clientHeight;
    dx = dx / (1. * width)
    dy = dy / (1. * height);

    const hkey = {id: random(), click: {x: dx, y: dy}};
    const hist = this.state.inputHistory;
    hist.push(hkey);
    this.setState({ inputHistory: hist });

    this.post(CLICK, {x: dx, y: dy});
  }

  render() {
    const devices = this.state.devices.map((device) => {
      return <MenuItem key={device} value={device} primaryText={device} />
    });

    var wwidth = this.state.windowWidth - 150;
    var wheight = this.state.windowHeight - 200;
    var wratio = wwidth / (1.0 * wheight);
    var sratio = this.state.screenshotWidth / (1.0 * this.state.screenshotHeight);
    var imgStyl = {width: 100, height: 100};

    if (sratio < wratio) {
      imgStyl = {
        width: wheight * sratio,
        height: wheight,
        opacity: !this.state.showNotReadyUi ? 1 : .5
      }
    } else {
      imgStyl = {
        width: wwidth,
        opacity: !this.state.showNotReadyUi ? 1 : .5
      }
      if (sratio > 0) {
        imgStyl.height = wwidth / sratio;
      }
    }

    return (
      <div style={CONTAINER_STYL}>
          <div>
              <SelectField
                  floatingLabelText="Devices"
                  value={this.state.selectedDevice}
                  onChange={this.handleDeviceChange.bind(this)}>
                  {devices}
              </SelectField>
          </div>
          <div>
            <div style={{padding: "30px 5px", display: "flex", justifyContent: "center"}}>
              <div hidden={!this.state.showNotReadyUi} style={{position: "absolute", alignSelf: "center"}}>
                <div className="single4"></div>
              </div>
              <div style={{display: "flex", padding: 0}}
                   onClick={this.onImgClick.bind(this)} >
                <img style={imgStyl}
                     src={this.state.screenshotPath}
                     onLoad={this.onLoad.bind(this)}/>
              </div>
            </div>
          </div>
          <InputHistoryRow input={this.state.inputHistory} />
      </div>
    );
  }
}

export default Home;
