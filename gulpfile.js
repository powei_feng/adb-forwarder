var path = require('path');
var del = require('del');
var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var server = require('./clip_server');
var exec = require('child_process').exec;

// set variable via $ gulp --type production
var environment = $.util.env.type || 'development';
var isProduction = environment === 'production';
var webpackConfig = require('./webpack.config.js').getConfig(environment);

var PORT = $.util.env.port || 1337;
var APP = 'app/';
var DIST = 'dist/';

// https://github.com/ai/autoprefixer
var autoprefixerBrowsers = [
  'ie >= 9',
  'ie_mob >= 10',
  'ff >= 30',
  'chrome >= 34',
  'safari >= 6',
  'opera >= 23',
  'ios >= 6',
  'android >= 4.4',
  'bb >= 10'
];

gulp.task('buildApp', function(cb) {
  exec('cd screen_mirror && ./gradlew assemble', (err, stdout, stderr) => {
    console.log(stdout);
    console.log(stderr);
    cb(err);
  });
});

gulp.task('scripts', function() {
  return gulp.src(webpackConfig.entry)
    .pipe($.webpack(webpackConfig))
    .pipe(isProduction ? $.uglify() : $.util.noop())
    .pipe(gulp.dest(DIST))
//    .pipe($.size({ title : 'js' }))
    .pipe($.connect.reload());
});

// copy html from app to dist
gulp.task('html', function() {
  return gulp.src(APP + 'index.html')
    .pipe(gulp.dest(DIST))
    .pipe($.size({ title : 'html' }))
    .pipe($.connect.reload());
});

gulp.task('styles',function(cb) {
    return gulp.src(APP + 'style.css')
        .pipe(gulp.dest(DIST));
});

gulp.task('serve', function() {
  server.start({
    root: DIST,
    port: PORT,
  });
});

// copy images
gulp.task('images', function(cb) {
  return gulp.src(APP + 'img/**/*.{png,jpg,jpeg,gif}')
    .pipe($.size({ title : 'img' }))
    .pipe(gulp.dest(DIST + 'img/'));
});

// watch styl, html and js file changes
gulp.task('watch', function() {
  gulp.watch(APP + 'stylus/*.styl', ['styles']);
  gulp.watch(APP + 'index.html', ['html']);
  gulp.watch(APP + 'scripts/**/*.js', ['scripts']);
  gulp.watch(APP + 'scripts/**/*.jsx', ['scripts']);
});

// remove bundels
gulp.task('clean', function(cb) {
  return del([DIST], cb);
});

// waits until clean is finished then builds the project
gulp.task('build', ['clean'], function(){
  gulp.start(['images', 'html', 'scripts', 'styles', 'buildApp']);
});
